<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\BarangController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(BarangController::class)->name('barang.')->prefix('master-data/barang')->group(function() {Route::get('/'
    ,
    'index')->name('index');
    Route::get('list'
    ,
    'listTabel')->name('list');
    
    Route::get('dalamfolder'
    ,
    'getDalamFolder')->name('fileDalamFolder');
    });


Route::controller(CustomerController::class)->name('customer.')->prefix('customer')->group(function() 
    {Route::get('/','list')->name('list');
    Route::get('/Input', 'formInput')->name('form-input');
    Route::post('/simpandata', 'simpanData')->name('simpan-data');
    });
