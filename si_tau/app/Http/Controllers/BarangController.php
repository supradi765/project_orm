<?php

namespace App\Http\Controllers;


use Illuminate\Routing\Controller as BaseController;

class BarangController extends BaseController
{
    public function index(){
        return view ('halaman_index');
    }
    public function listTabel(){
        $data_sayur = "bayem, sawi, kol, kangkung, kelor";

        // data string
        $notif_halo = "selamat datang";
        // data number
        $data_number = 1234567890;
        // data bolean
        $data_bolean = true;

        // data array
        $data_barang = ["apel", "anggur", "Pisang", "Melon"];

        return view('list_tabel_barang', compact('data_sayur',  'notif_halo', 'data_number', 'data_bolean', 'data_barang' ) );
    }
    public function getDalamFolder(){
        return view('barang.halaman_didalam_folder');
    }
}
