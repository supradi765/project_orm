<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('gudang', function (Blueprint $table) {
            $table->text('alamat_gudang');
            $table->text('alamat_dua');
            $table->text('alamat_tiga');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('gudang', function (Blueprint $table) {
            $table->dropColumn('alamat_gudang');
            $table->droupColumn('alamat_dua');
            $table->droupColumn('alamat_tiga');
        });
    }
};
